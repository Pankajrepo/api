import express from 'express'
import { preQuery } from '../../utils'
import auth from '../system/middleware'
import paramValidation from '../validator'
import SampleCtrl from './controller'

const router = express.Router()

router.post('/', paramValidation.brand.validateCreateSample, auth.requiresBusiness, SampleCtrl.create)

router.get('/', paramValidation.brand.validateListSamples, auth.requiresBusiness, SampleCtrl.index)

router.get('/:id', auth.requiresBusiness, SampleCtrl.show)

router.put('/:id', paramValidation.brand.validateUpdateSample, auth.requiresSampleInBusiness, SampleCtrl.update)

router.patch('/:id', auth.requiresSampleInBusiness, SampleCtrl.changeStatus)

router.delete('/', paramValidation.product.validateDeleteMulti, auth.requiresSamplesInBusiness, SampleCtrl.destroy)

router.param('id', preQuery.brand)

export default router

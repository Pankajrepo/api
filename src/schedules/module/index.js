import { CronJob } from 'cron'

const every5Second = '*/5 * * * * *'
const job = new CronJob({
  cronTime: every5Second,
  onTick: () => {
    init()
  },
  timeZone: 'Asia/Ho_Chi_Minh'
})

function init() {

}
job.start()

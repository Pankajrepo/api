import sendEmail from '@sendgrid/mail'
import i18n from 'i18n'

sendEmail.setApiKey(process.env.SENDGIRD_API_KEY)
const emailAddress = process.env.SENDGIRD_EMAIL_ADDRESS

const defaultMsg = () => {
  return {
    to: '',
    from: emailAddress,
    subject: i18n.__('sendEmail.subject'),
    text: i18n.__('sendEmail.text'),
    html: ''
  }
}

const renderOTPCode = () => {
  return [...Array(6)].map(() => Math.random().toString(36)[2]).join('').toLocaleUpperCase()
}

const renderHTMLMsg = async (code, textTitle = i18n.__('sendEmail.titleSendOPTCode')) => {
  return `<div style="text-align: center">
    <h3 style="background-color: cornflowerblue; color: white; text-align: center; padding: 20px">${textTitle}</h3>
    <h2>${code}</h2>
  </div>`
}


const renderHTMLForgotPasswordMsg = async (code, textTitle = i18n.__('sendEmail.forgotPasswordOTPCode')) => {
  return `<div style="text-align: center">
    <h3 style="background-color: cornflowerblue; color: white; text-align: center; padding: 20px">${textTitle}</h3>
    <h2>${code}</h2>
  </div>`
}

const sendEmailOtpCode = async (email, code, isResendOtpCode) => {
  return new Promise(async (resolve, reject) => {
    code = code || renderOTPCode()
    let renderMsg = await renderHTMLMsg(code)
    if (isResendOtpCode) {
      renderMsg = await renderHTMLMsg(code, i18n.__('sendEmail.titleResendOPTCode'))
    }
    const msg = Object.assign(defaultMsg(), {
      to: email,
      html: renderMsg
    })
    sendEmail.send(msg, (error) => {
      if (error) {
        return reject(error)
      }
      resolve(true)
    })
  })
}

const sendEmailOtpResetPassword = async (email, code, isResendOtpCode) => {
  return new Promise(async (resolve, reject) => {
    code = code || renderOTPCode()
    let renderMsg = await renderHTMLForgotPasswordMsg(code)
    if (isResendOtpCode) {
      renderMsg = await renderHTMLForgotPasswordMsg(code, i18n.__('sendEmail.titleResendOPTCode'))
    }
    const msg = Object.assign(defaultMsg(), {
      to: email,
      html: renderMsg
    })
    sendEmail.send(msg, (error) => {
      if (error) {
        return reject(error)
      }
      resolve(true)
    })
  })
}

export {
  sendEmailOtpCode,
  sendEmailOtpResetPassword
}

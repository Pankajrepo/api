import { Twilio } from 'twilio'
import { parsePhoneNumberFromString } from 'libphonenumber-js'

const accountSid = process.env.TWILIO_ACCOUNT_SID
const authToken = process.env.TWILIO_AUTH_TOKEN
const fromPhone = process.env.TWILIO_NUMBER
const client = new Twilio(accountSid, authToken)

const sendSMS = async (toPhoneNumber, codeVerify) => {
  try {
    await client.messages.create({
      body: `Dual-lite code verify: ${codeVerify}`,
      from: fromPhone || 'Dual-lite',
      to: toPhoneNumber
    })
    return {
      status: 'success'
    }
  } catch (err) {
    console.log('error when send sms: ', err);
    return {
      status: 'error'
    }
  }
}

const checkPhoneFormat = async (phone) => {
  // TODO check Twilio ACCOUNT
  const result = await client.lookups.phoneNumbers(phone)
    .fetch({ type: ['carrier'] })
    .then((success) => { return success.phoneNumber })
    .catch((error) => {
      console.log(error)
      return undefined
    })
  return result
}
const parsePhoneNumber = (phone) => {
  const { number } = parsePhoneNumberFromString(addPlusToPhoneString(phone))
  return number
}

function addPlusToPhoneString(phone) {
  if (!phone) {
    return ''
  }
  if (phone.includes('+')) {
    return phone
  } else {
    return `+${phone}`
  }
}

module.exports = {
  sendSMS,
  checkPhoneFormat,
  parsePhoneNumber
}

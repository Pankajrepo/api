/* eslint-disable no-unused-vars */
import lodash from 'lodash'
import i18n from 'i18n'
import { handleResponse } from '../../utils/handle-response';
import to from '../../utils/to'
import service from './service'
import config from './config'
import { getContentLanguage } from '../../locales'


async function sendSMSCode(req, res) {
  const body = lodash.pick(req.body, 'phone');
  const [error, result] = await to(service.sendSMSCode(body));
  return handleResponse(error, result, req, res);
}

async function verifyCode(req, res) {
  const ALLOWED_ATTRIBUTE = ['phone', 'code'];
  const body = lodash.pick(req.body, ALLOWED_ATTRIBUTE);
  const [error, customer] = await to(service.verifySMSCode(body));
  return handleResponse(error, customer, req, res);
}

async function signUp(req, res) {
  const body = lodash.pick(req.body, config.ALLOWED_CREATE_ATTRIBUTE);
  body.locale = getContentLanguage(req)
  const [error, result] = await to(service.signUp(body));
  return handleResponse(error, result, req, res);
}

async function login(req, res) {
  const body = lodash.pick(req.body, config.ALLOWED_LOGIN_ATTRIBUTE);
  body.locale = getContentLanguage(req)
  const [error, result] = await to(service.loginPhone(body));
  return handleResponse(error, result, req, res);
}

async function show(req, res) {
  const [error, result] = await to(service.show(req.params.id));
  return handleResponse(error, result, req, res);
}

async function update(req, res) {
  const [error, customer] = await to(service.update(req.params.id, req.body));
  handleResponse(error, customer, req, res);
}


async function logout(req, res) {
  const body = lodash.pick(req.body, config.ALLOWED_LOGOUT_ATTRIBUTE);
  const [error, customer] = await to(service.logout(body, req.user));
  handleResponse(error, customer, req, res);
}


async function sendEmailVerify(req, res) {
  const body = lodash.pick(req.body, 'email');
  const [error, result] = await to(service.sendEmailVerify(body));
  return handleResponse(error, result, req, res);
}

async function verifyEmail(req, res) {
  const ALLOWED_ATTRIBUTE = ['email', 'code'];
  const body = lodash.pick(req.body, ALLOWED_ATTRIBUTE);
  const [error, customer] = await to(service.verifyEmail(body));
  return handleResponse(error, customer, req, res);
}


async function forgotPassword(req, res) {
  const body = lodash.pick(req.body, ['email']);
  const [error, result] = await to(service.forgotPassword(body));
  return handleResponse(error, result, req, res);
}


async function checkExistAccount(req, res) {
  const body = lodash.pick(req.body, 'email', 'phone');
  const [error, customer] = await to(service.checkExistAccount(body));
  handleResponse(error, customer, req, res);
}


const resetPassword = async (req, res) => {
  const body = lodash.pick(req.body, config.ALLOWED_RESET_PASSWORD_ATTRIBUTE)
  const [error, data] = await to(service.resetPassword(body))
  handleResponse(error, data, req, res)
}


const updateLocale = async (req, res) => {
  const locales = i18n.getLocales();
  const [error, data] = await to(service.updateLocale(req.user, locales, req.params.lang))
  if (data) {
    i18n.setLocale(req.params.lang);
    res.cookie('lang', req.params.lang);
  }
  handleResponse(error, data, req, res)
}


export default {
  verifyCode,
  signUp,
  logout,
  login,
  update,
  sendSMSCode,
  show,
  sendEmailVerify,
  verifyEmail,
  forgotPassword,
  checkExistAccount,
  resetPassword,
  updateLocale
};


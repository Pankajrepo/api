import { mongoose, Schema } from '../../utils/mongoose'
import statics from './static'
import config from './config'

const VerifySMSCodeSchema = new Schema({
  phone: {
    type: String,
    required: true,
    unique: true
  },
  code: {
    type: String,
    default: null
  },
  match: {
    type: Number,
    enum: config.matchCodeList,
    required: true
  },
  date: {
    type: Date,
    required: true
  },
}, {
  versionKey: false,
  timestamps: true
});
VerifySMSCodeSchema.statics = statics;
VerifySMSCodeSchema.index({ code: 1 })

export default mongoose.model('VerifySMSCode', VerifySMSCodeSchema, 'verify_sms_code')

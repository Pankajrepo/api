import configCommon from '../common/config'

const buildUpdateUserAfterLogin = function (resultToken = {}, body = {}) {
  const rs = {
    lastLogin: new Date().toISOString(),
    accessToken: resultToken.accessToken,
    deviceType: body.deviceType,
    deviceToken: body.deviceToken,
    appVersion: body.appVersion
  }
  if (body.locale && configCommon.languageList.includes(body.locale)) {
    rs.locale = body.locale
  }

  return rs
}

export {
  buildUpdateUserAfterLogin
}

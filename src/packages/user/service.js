/* eslint-disable eqeqeq */
/* eslint-disable no-unused-vars */
import lodash from 'lodash';
import moment from 'moment'
import repo from './repository';
import config from './config';
import configCommon from '../common/config';
import { buildUpdateUserAfterLogin } from './query-builder'
import method from './method'
import { checkPhoneFormat, sendSMS, parsePhoneNumber } from '../../services/phoneService';
import { sendEmailOtpCode, sendEmailOtpResetPassword } from '../../services/emailService';
import { genTokenObject } from '../../utils/gen-token';
import { genCodePhone } from '../../utils/random-code';
import { commonLocale, validationLocale } from '../../locales'
import to from '../../utils/to'
import redisUtil from '../../utils/redisUtil'

/**
 * Verify phone number service
 * @param {*} body
 */
async function verifySMSCode(body) {
  return new Promise(async (resolve, reject) => {
    if (!body) {
      return reject(validationLocale.phone.phoneInvalid);
    }
    const validPhone = parsePhoneNumber(body.phone) // await checkPhoneFormat(body.phone);
    if (validPhone === undefined) {
      return reject(validationLocale.phone.phoneInvalid);
    }
    const phone = await repo.findOneVerifySMSCode({
      phone: validPhone,
      match: config.matchCode.notverified
    });
    if (!phone) {
      return reject(validationLocale.phone.incorrectCode);
    } else {
      if (phone.code !== body.code) {
        return reject(validationLocale.phone.incorrectCode);
      }
      if (Date.now() > new Date(phone.date).getTime() + config.expiredTime.time) {
        return reject(validationLocale.phone.expiredCode);
      }
      phone.match = config.matchCode.verified;
      phone.code = null;
      repo.updateOneVerifySMSCode(phone._id, phone)
      return resolve({
        valid: true,
        phone: validPhone
      });
    }
  })
}

async function sendSMSCode(body) {
  const validPhone = parsePhoneNumber(body.phone)
  // const validPhone = await checkPhoneFormat(body.phone);
  // if (validPhone === undefined) {
  //   throw new Error(JSON.stringify(validationLocale.phone.phoneInvalid));
  // }
  const code = 1111 // genCodePhone();
  // const statusSent = await sendSMS(validPhone, code);
  // if (statusSent.status === 'error') {
  //   throw new Error(JSON.stringify((validationLocale.phone.sendSMSFail)))
  // }
  const existedPhone = await repo.findOneVerifySMSCode({
    phone: validPhone
  });
  if (!existedPhone) {
    const newDataPhone = {
      phone: validPhone,
      code,
      match: config.matchCode.notverified,
      date: Date.now()
    };
    const newPhone = await repo.createVerifySMSCode(newDataPhone);
    const dataReturn = lodash.pick(newPhone, ['phone']);
    return dataReturn;
  }
  existedPhone.code = code;
  existedPhone.match = config.matchCode.notverified;
  existedPhone.date = Date.now();
  repo.updateOneVerifySMSCode(existedPhone._id, {
    code,
    match: config.matchCode.notverified,
    date: Date.now()
  });
  const dataReturn = lodash.pick(existedPhone, ['phone']);
  return dataReturn;
}


/**
 * Check existed account phone or email
 * @param {*} body
 */
async function checkExistAccount(body) {
  const phoneUser = await repo.findOne({ phone: parsePhoneNumber(body.phone) });
  if (phoneUser) {
    throw new Error(JSON.stringify(validationLocale.phone.existedPhone))
  }
  const emailUser = await repo.findOne({ email: body.email });
  if (emailUser) {
    throw new Error(JSON.stringify(validationLocale.email.existedEmail))
  }
  return true
}
/**
 * Sign up by phone
 * @param {*} body
 */
async function signUp(body) {
  const user = await repo.findOne({
    $or: [{
      phone: parsePhoneNumber(body.phone),
    }, {
      email: body.email
    }]
  });
  if (user) {
    throw new Error(JSON.stringify(validationLocale.user.existUser))
  }

  const [errorCode, resultCode] = await to(verifySMSCode(body))
  if (errorCode) {
    throw new Error(JSON.stringify(errorCode));
  }

  const [error, { _doc }] = await to(repo.create(body));
  if (error) {
    throw new Error(JSON.stringify(commonLocale.somethingWrong))
  }
  const resultToken = genTokenObject(_doc._id, _doc.phone);
  repo.updateOne(_doc._id, {
    accessToken: resultToken.accessToken
  })
  delete _doc.password
  const authenData = { ..._doc, ...resultToken }
  saveAuthenInfoCache(_doc._id, authenData)

  return authenData
}

async function loginPhone(body) {
  const user = await repo.findOneExistPassword({ phone: parsePhoneNumber(body.phone) });
  if (!user) {
    throw new Error(JSON.stringify(commonLocale.loginFailed));
  }
  if (user.status === config.status.inactive) {
    throw new Error(JSON.stringify(validationLocale.user.accountBlocked));
  }

  if (!method.comparePassword(body.password, user.password)) {
    throw new Error(JSON.stringify((commonLocale.loginFailed)));
  }

  const resultToken = genTokenObject(user._id, user.phone);
  const [error, updated] = await to(repo.updateOne(user._id, buildUpdateUserAfterLogin(resultToken, body)))
  if (error || !updated) {
    throw new Error(JSON.stringify(commonLocale.somethingWrong));
  }

  const authenData = { ...updated, ...resultToken }
  saveAuthenInfoCache(updated._id, authenData)

  return authenData
}

async function logout(body, user) {
  repo.updateOne(user._id, {
    accessToken: null,
    deviceToken: null
  })
  delAuthCache(user._id)

  return true
}


async function update(id, body) {
  delAuthCache(id)

  return repo.updateOne(id, body)
}

async function show(id) {
  return repo.findById(id)
}

async function loginByEmail(body) {
  const user = await repo.findOneExistPassword({ email: body.email });
  if (!user) {
    throw new Error(JSON.stringify(commonLocale.loginFailed));
  }
  if (user.status === config.status.inactive) {
    throw new Error(JSON.stringify(validationLocale.user.accountBlocked));
  }

  if (!method.comparePassword(body.password, user.password)) {
    throw new Error(JSON.stringify((commonLocale.loginFailed)));
  }

  const resultToken = genTokenObject(user._id, user.phone);
  const [error, updated] = await to(repo.updateOne(user._id, buildUpdateUserAfterLogin(resultToken, body)))
  if (error || !updated) {
    throw new Error(JSON.stringify(commonLocale.somethingWrong));
  }

  const authenData = { ...updated, ...resultToken };
  saveAuthenInfoCache(updated._id, authenData)

  return authenData
}


async function sendEmailVerify(body) {
  const code = genCodePhone();
  const [error, successSent] = await to(sendEmailOtpCode(body.email, code));
  if (!successSent) {
    throw new Error(JSON.stringify((validationLocale.email.sendEmailFail)))
  }
  const dataReturn = await saveEmailVerifyCode(body.email, code);
  return dataReturn;
}


async function sendEmailToResetPassword(email) {
  const code = genCodePhone();
  const [error, successSent] = await to(sendEmailOtpResetPassword(email, code));
  if (!successSent) {
    throw new Error(JSON.stringify((validationLocale.email.sendEmailFail)))
  }
  const dataReturn = await saveEmailVerifyCode(email, code);
  return dataReturn;
}

const saveEmailVerifyCode = async (email, code) => {
  const existedEmail = await repo.findOneVerifyEmailCode({
    email
  });
  if (!existedEmail) {
    const newDataVerify = {
      email,
      code,
      match: config.matchCode.notverified,
      date: Date.now()
    };
    const newData = await repo.createVerifyEmailCode(newDataVerify);
    const dataReturn = lodash.pick(newData, ['email']);
    return dataReturn;
  }
  existedEmail.code = code;
  existedEmail.match = config.matchCode.notverified;
  existedEmail.date = Date.now();
  repo.updateOneVerifyEmailCode(existedEmail._id, {
    code,
    match: config.matchCode.notverified,
    date: Date.now()
  });
  const dataReturn = lodash.pick(existedEmail, ['email']);
  return dataReturn
}

async function verifyEmail(body) {
  return new Promise(async (resolve, reject) => {
    const modelVerify = await repo.findOneVerifyEmailCode({
      email: body.email,
      match: config.matchCode.notverified
    });
    if (!modelVerify) {
      return reject(validationLocale.email.incorrectCode);
    } else {
      if (modelVerify.code !== body.code) {
        return reject(validationLocale.email.incorrectCode);
      }
      if (Date.now() > new Date(modelVerify.date).getTime() + config.expiredTime.time) {
        return reject(validationLocale.email.expiredCode);
      }
      modelVerify.match = config.matchCode.verified;
      modelVerify.code = null;
      repo.updateOneVerifyEmailCode(modelVerify._id, modelVerify)
      return resolve({
        valid: true,
        email: body.email,
      });
    }
  })
}

const forgotPassword = async (body) => {
  const user = await repo.findOneExistPassword({ email: body.email });
  if (user) {
    sendEmailToResetPassword(body.email)
  }
  return {
    email: body.email
  }
}


const resetPassword = async (body) => {
  const [errorCode, resultCode] = await to(verifySMSCode(body))
  if (errorCode) {
    throw new Error(JSON.stringify(errorCode));
  }
  const userModel = await repo.findOneExistPassword({
    phone: resultCode.phone
  });
  if (!userModel) {
    throw new Error(JSON.stringify(validationLocale.email.emailNoneRegister))
  }

  const resultToken = genTokenObject(userModel._id, userModel.email);
  const updateQuery = buildUpdateUserAfterLogin(resultToken, body)
  updateQuery.password = method.hashInputPassword(body.password)
  const [error, updated] = await to(repo.updateOne(userModel._id, updateQuery))
  if (error || !updated) {
    throw new Error(JSON.stringify(commonLocale.somethingWrong));
  }

  const authenData = { ...updated, ...resultToken };
  saveAuthenInfoCache(updated._id, authenData)

  return authenData
}

const blockUser = async (userId) => {
  return repo.updateOne(userId, {
    blockExpires: moment().add(config.HOURS_TO_BLOCK, 'hours'),
    $inc: {
      loginAttempts: +1
    }
  })
}

/**
 * Checks if blockExpires from user is greater than now
 * @param {Object} user - user object
 */
const userIsBlocked = async (blockExpires) => {
  return moment(blockExpires).isAfter(moment())
}

const blockIsExpired = (user) => {
  return user.loginAttempts > config.LOGIN_ATTEMPTS && user.blockExpires <= new Date()
}

const updateLocale = async (user, locales, locale) => {
  if (locales.includes(locale)) {
    if (user.locale !== locale) {
      repo.updateOne(user._id, { locale })
      delAuthCache(user._id)
    }
    return {
      lang: locale
    }
  } else {
    throw new Error(JSON.stringify(commonLocale.notFound));
  }
}


/**
 * Get authentication infomation from cache. If not exist then query from db
 */
const getAuthenInfo = async (userId) => {
  const key = `${config.authTokenCache}${userId}`
  let user = await redisUtil.getKey(key)
  if (user && user._id) {
    return user
  } else {
    user = await repo.getDataAuthenInfo(userId);
    if (user) {
      saveAuthenInfoCache(userId, user)
    }

    return user;
  }
}

const saveAuthenInfoCache = async (userId, data) => {
  const key = `${config.authTokenCache}${userId}`
  redisUtil.writeKey(key, data, config.AUTHEN_CACHE_EXPIRE).catch((error) => {
    console.log(`Error Save Authentication Cache: ${error}`)
  })
}

const delAuthCache = async (userId) => {
  const key = `${config.authTokenCache}${userId}`
  redisUtil.purgeKey(key).catch((error) => {
    console.log(`Error Del Authentication Cache: ${error}`)
  })
}


const loginSocial = async (type = config.accountType.facebook, body) => {

}


export default {
  sendSMSCode,
  verifySMSCode,
  loginPhone,
  logout,
  update,
  signUp,
  show,
  sendEmailVerify,
  verifyEmail,
  forgotPassword,
  checkExistAccount,
  resetPassword,
  updateLocale,
  getAuthenInfo
};

import express from 'express'
import paramValidation from '../validator'
import Ctrl from './controller'
import authentication from '../../middleware/authMiddleWare'

const userRouter = express.Router();

userRouter.get('/', authentication, (req, res, next) => {
  req.params.id = req.user._id
  next()
}, Ctrl.show);

userRouter.patch('/', authentication, paramValidation.user.update, (req, res, next) => {
  req.params.id = req.user._id
  next()
}, Ctrl.update);

const authRouter = express.Router();

authRouter.post('/signUp', paramValidation.user.signUp, Ctrl.signUp);

authRouter.post('/login', paramValidation.user.login, Ctrl.login);

authRouter.post('/sendSMSCode', paramValidation.user.sendSMSCode, Ctrl.sendSMSCode);

authRouter.post('/verifySMSCode', paramValidation.user.verifyCode, Ctrl.verifyCode);

authRouter.post('/sendEmailVerify', paramValidation.user.sendEmailVerify, Ctrl.sendEmailVerify);

authRouter.post('/verifyEmail', paramValidation.user.verifyEmail, Ctrl.verifyEmail);

authRouter.post('/forgotPwd', paramValidation.user.forgotPwd, Ctrl.forgotPassword);

authRouter.post('/resetPwd', paramValidation.user.resetPwd, Ctrl.resetPassword);

authRouter.post('/checkExist', paramValidation.user.checkExisted, Ctrl.checkExistAccount);

authRouter.post('/logout', authentication, paramValidation.user.logout, Ctrl.logout);


export {
  userRouter,
  authRouter
}

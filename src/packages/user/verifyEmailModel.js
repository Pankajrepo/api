import { mongoose, Schema } from '../../utils/mongoose'
import statics from './static'
import config from './config'

const VerifyEmailCodeSchema = new Schema({
  email: {
    type: String,
    required: true,
    unique: true
  },
  code: {
    type: String,
    default: null
  },
  match: {
    type: Number,
    enum: config.matchCodeList,
    required: true
  },
  date: {
    type: Date,
    required: true
  },
}, {
  versionKey: false,
  timestamps: true
});

VerifyEmailCodeSchema.index({ code: 1 })

VerifyEmailCodeSchema.statics = statics;

export default mongoose.model('VerifyEmailCode', VerifyEmailCodeSchema, 'verify_email_code')

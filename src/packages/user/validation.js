import Joi from 'joi'
import configs from '../../configs'
import config from './config'
import errorMessage from '../../utils/custom-error-message'

export default {
  signUp: {
    body: {
      phone: Joi.string().regex(configs.regex.phone).required().error(errorMessage()),
      code: Joi.string().required().error(errorMessage()),
      password: Joi.string().required().error(errorMessage()),
      email: Joi.string().required().regex(configs.regex.email).error(errorMessage()),
      deviceToken: Joi.string().error(errorMessage()),
      appVersion: Joi.string().error(errorMessage()),
      deviceType: Joi.string().valid(config.deviceTypeList).required().error(errorMessage()),
      accountType: Joi.string().valid(config.accountType).error(errorMessage()),
      fullName: Joi.string().required().error(errorMessage())
    }
  },
  login: {
    body: {
      phone: Joi.string().regex(configs.regex.phone).required().error(errorMessage()),
      password: Joi.string().required().error(errorMessage()),
      deviceToken: Joi.string().error(errorMessage()),
      appVersion: Joi.string().error(errorMessage()),
      deviceType: Joi.string().valid(config.deviceTypeList).required().error(errorMessage())
    }
  },
  loginEmail: {
    body: {
      email: Joi.string().regex(configs.regex.email).required().error(errorMessage()),
      password: Joi.string().required().error(errorMessage()),
      deviceToken: Joi.string().error(errorMessage()),
      deviceType: Joi.string().valid(config.deviceTypeList).required().error(errorMessage()),
      appVersion: Joi.string().error(errorMessage())
    }
  },
  sendSMSCode: {
    body: {
      phone: Joi.string().regex(configs.regex.phone).required().error(errorMessage())
    }
  },
  verifyCode: {
    body: {
      phone: Joi.string().regex(configs.regex.phone).required().error(errorMessage()),
      code: Joi.string().required().error(errorMessage())
    }
  },
  sendEmailVerify: {
    body: {
      email: Joi.string().regex(configs.regex.email).required().error(errorMessage())
    }
  },
  verifyEmail: {
    body: {
      email: Joi.string().regex(configs.regex.email).required().error(errorMessage()),
      code: Joi.string().required().error(errorMessage())
    }
  },
  update: {
    body: {
      avatar: Joi.string().allow([null, '']).error(errorMessage()),
      gender: Joi.string().valid(config.genderList).error(errorMessage()),
      fullName: Joi.string().error(errorMessage()),
      birthDay: Joi.string().error(errorMessage()),
      country: Joi.string().error(errorMessage()),
      region: Joi.string().error(errorMessage()),
      area: Joi.string().error(errorMessage()),
      interested: Joi.string().valid(config.genderList).error(errorMessage()),
      interest: Joi.array().error(errorMessage()),
      age: Joi.number().error(errorMessage()),
      invitation: Joi.string().allow([null, '']).error(errorMessage()),
      upcoming: Joi.string().allow([null, '']).error(errorMessage()),
      orientation: Joi.string().error(errorMessage())
    }
  },
  logout: {
    body: {
      phone: Joi.string().regex(configs.regex.phone).error(errorMessage()),
      email: Joi.string().regex(configs.regex.email).error(errorMessage()),
      accessToken: Joi.string().error(errorMessage()),
      deviceToken: Joi.string().error(errorMessage()),
      appVersion: Joi.string().error(errorMessage()),
      deviceType: Joi.string().valid(config.deviceTypeList).error(errorMessage())
    }
  },
  forgotPwd: {
    body: {
      phone: Joi.string().regex(configs.regex.email).required().error(errorMessage()),
    }
  },
  resetPwd: {
    body: {
      code: Joi.string().required().error(errorMessage()),
      phone: Joi.string().regex(configs.regex.phone).required().error(errorMessage()),
      password: Joi.string().required().error(errorMessage()),
      deviceToken: Joi.string().error(errorMessage()),
      appVersion: Joi.string().error(errorMessage()),
      deviceType: Joi.string().valid(config.deviceTypeList).required().error(errorMessage())
    }
  },
  checkExisted: {
    body: {
      phone: Joi.string().regex(configs.regex.phone).required().error(errorMessage()),
      email: Joi.string().regex(configs.regex.email).error(errorMessage()),
    }
  }
}

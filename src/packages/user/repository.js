import { User, VerifySMSCode, VerifyEmailCode } from '../../models';
import { ObjectId } from '../../utils/mongoose'

async function createVerifySMSCode(body) {
  return VerifySMSCode.create(body)
}

async function findOneVerifySMSCode(query) {
  return VerifySMSCode.findOne(query)
}

async function updateOneVerifySMSCode(id, data) {
  return VerifySMSCode.findOneAndUpdate(
    { _id: new ObjectId(id) },
    data,
    { new: true }
  )
}

async function createVerifyEmailCode(body) {
  return VerifyEmailCode.create(body)
}

async function findOneVerifyEmailCode(query) {
  return VerifyEmailCode.findOne(query)
}

async function updateOneVerifyEmailCode(id, data) {
  return VerifyEmailCode.findOneAndUpdate(
    { _id: new ObjectId(id) },
    data,
    { new: true }
  )
}
/**
 * Get Data From Server And Save Authentication Cache
 * @param {*} id
 */
async function getDataAuthenInfo(id) {
  return User.findById(new ObjectId(id)).select('-password').lean();
}

/**
 * @param {*} isFullFields Boolean
 */
async function findById(id, isFullFields) {
  if (isFullFields) {
    return User.findById(new ObjectId(id)).lean();
  } else {
    return User.findById(new ObjectId(id)).select('-password -accessToken').lean();
  }
}

async function find(query) {
  return User.find(query).select('-password').lean();
}

async function findOne(query) {
  return User.findOne(query).select('-password').lean();
}

async function findOneExistPassword(query) {
  return User.findOne(query).lean();
}

async function create(body) {
  return User.create(body)
}


async function updateOne(id, data) {
  return User.findOneAndUpdate(
    { _id: new ObjectId(id) },
    data,
    { new: true }
  ).select('-password -accessToken').lean();
}

async function destroy(id) {
  return User.deleteOne({ _id: new ObjectId(id) })
}


export default {
  find,
  create,
  findOne,
  findOneExistPassword,
  updateOne,
  destroy,
  createVerifySMSCode,
  findOneVerifySMSCode,
  updateOneVerifySMSCode,
  createVerifyEmailCode,
  findOneVerifyEmailCode,
  updateOneVerifyEmailCode,
  findById,
  getDataAuthenInfo
}

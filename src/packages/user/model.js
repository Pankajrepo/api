import { mongoose, Schema } from '../../utils/mongoose'
import statics from './static'
import hooks from './hook'
import methods from './method'
import config from './config'
import configCommon from '../common/config'

const UserSchema = new Schema({
  phone: {
    type: String,
    unique: true,
  },
  email: {
    type: String,
    unique: true,
    required: true
  },
  isVerifiedEmail: {
    type: Boolean,
    default: false
  },
  password: {
    type: String,
    required: true
  },
  firstName: String,
  lastName: String,
  fullName: String,
  gender: {
    type: String,
    enum: config.genderList
  },
  birthDay: String,
  avatar: String,
  country: String,
  countryCode: {
    type: String,
    lowercase: true
  },
  region: String,
  regionCode: String,
  area: String,
  interested: {
    type: String,
    enum: config.genderList
  },
  interest: [String],
  age: Number,
  invitation: String,
  upcoming: String,
  orientation: {
    type: String,
    default: config.orientation.straight,
    enum: config.orientationList
  },
  locale: {
    type: String,
    default: configCommon.language.en,
    enum: configCommon.languageList
  },
  lastLogin: Date,
  accountType: {
    type: String,
    default: config.accountType.normal,
    enum: config.accountTypeList
  },
  status: {
    type: Number,
    default: config.status.active,
    enum: config.statusList
  },
  accessToken: String,
  deviceToken: String,
  deviceType: {
    type: String,
    enum: config.deviceTypeList
  },
  appVersion: String,
  location: {
    type: {
      type: String,
      enum: ['Point']
    },
    coordinates: {
      type: [Number]
    }
  },
  scanReference: String,
  scanInfo: Schema.Types.Mixed
}, {
  versionKey: false,
  timestamps: true
})

UserSchema.statics = statics;

UserSchema.methods = methods

UserSchema.index({ location: '2dsphere' })
UserSchema.index({ createdAt: -1 })
UserSchema.index({ status: 1 })
UserSchema.index({ locale: 1 })
UserSchema.index({ fullName: 1 })
UserSchema.index({ age: 1 })

UserSchema.pre('save', function (next) {
  hooks.genHashPassword(this, next)
  hooks.preSaveHookCode(this, 'regionCode', next)
})

export default mongoose.model('User', UserSchema, 'users')

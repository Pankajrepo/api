import stringUtil from '../../utils/stringUtil'

function genHashPassword(doc, next) {
  if (doc.isNew) {
    doc.genHashPassword()
  }
  next()
}

function preSaveHookCode(doc, code, next) {
  if (!doc[code]) {
    doc[code] = stringUtil.changeAlias(doc.region)
  }
  next()
}
export default {
  genHashPassword,
  preSaveHookCode
}

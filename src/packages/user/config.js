const authTokenCache = 'authen:token_'

const HOURS_TO_BLOCK = 2
const LOGIN_ATTEMPTS = 5
const AUTHEN_CACHE_EXPIRE = 30 * 24 * 60 * 60

const deviceType = {
  android: 'android',
  ios: 'ios'
}

const deviceTypeList = Object.values(deviceType)

const status = {
  active: 1,
  inactive: 2
}

const statusList = Object.values(status)

const gender = {
  male: 'male',
  female: 'female',
  other: 'other'
}
const genderList = Object.values(gender)

const type = {
  resident: 'resident',
  tourist: 'tourist'
}
const typeList = Object.values(type)

const loginType = {
  phone: 'phone',
  email: 'email'
}
const loginTypeList = Object.values(loginType)

const matchCode = {
  verified: 1,
  notverified: 2
}

const matchCodeList = Object.values(matchCode)

const orientation = {
  bisexual: 'bisexual',
  straight: 'straight'
}

const orientationList = Object.values(orientation)


const accountType = {
  normal: 'normal',
  facebook: 'facebook'
}

const accountTypeList = Object.values(accountType)

const expiredTime = { // Expiry time verify code
  time: 5 * 60 * 1000
}


const ALLOWED_CREATE_ATTRIBUTE = [
  'phone',
  'code',
  'email',
  'deviceToken',
  'appVersion',
  'deviceType',
  'password'
];

const ALLOWED_LOGIN_ATTRIBUTE = [
  'phone',
  'email',
  'password',
  'code',
  'deviceType',
  'appVersion',
  'deviceToken',
  'locale'
];

const ALLOWED_LOGOUT_ATTRIBUTE = [
  'email',
  'phone',
  'accessToken',
  'deviceType',
  'deviceToken'
];

const ALLOWED_RESET_PASSWORD_ATTRIBUTE = [
  'phone',
  'email',
  'code',
  'password',
  'appVersion',
  'deviceType',
  'deviceToken'
];


export default {
  limit: {
    index: 20
  },
  status,
  statusList,
  gender,
  genderList,
  type,
  typeList,
  matchCode,
  matchCodeList,
  expiredTime,
  deviceType,
  deviceTypeList,
  loginType,
  loginTypeList,
  authTokenCache,
  orientation,
  orientationList,
  accountType,
  accountTypeList,
  ALLOWED_LOGIN_ATTRIBUTE,
  ALLOWED_LOGOUT_ATTRIBUTE,
  ALLOWED_CREATE_ATTRIBUTE,
  HOURS_TO_BLOCK,
  LOGIN_ATTEMPTS,
  ALLOWED_RESET_PASSWORD_ATTRIBUTE,
  AUTHEN_CACHE_EXPIRE
}

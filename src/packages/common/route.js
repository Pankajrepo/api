import express from 'express';
import authentication from '../../middleware/authMiddleWare'
import userController from '../user/controller'

const router = express.Router();

/**
 * @api {get} /language/:lang Change Language
 *
 * @apiGroup Change Language
 * @apiName Change Language
 * @apiDescription Change Language
 * @apiVersion 1.0.0
 *
 * @apiParam {String} [lang = en:English,ar:Arabic] Code language
 */
router.get('/language/:lang', authentication, userController.updateLocale);

export default router

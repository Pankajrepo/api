
const mediaType = {
  image: 'image',
  video: 'video'
}

const mediasType = [mediaType.image, mediaType.video]

export {
  mediaType,
  mediasType
}

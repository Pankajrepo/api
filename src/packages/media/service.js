import map from 'apr-map'
import repo from './repository'
import { mediaType } from './config';

const uploadImage = async (images, user) => {
  const dataMedia = [];
  await map.series(images, async (image) => {
    const data = {
      type: mediaType.image,
      user
    }
    await map.series(image.transforms, async (transform) => {
      switch (transform.id) {
        case 'origin':
          data.origin = transform.location
          break;
        case 'feature':
          data.feature = transform.location
          break;
        case 'thumbnail':
          data.thumbnail = transform.location
          break;
        case 'avatar':
          data.avatar = transform.location
          break;
        default:
          break;
      }
    })
    await dataMedia.push(data)
  });
  return repo.createMedia(dataMedia);
}

const index = async (request) => {
  return repo.findAll(request)
}
export default {
  uploadImage,
  index
}


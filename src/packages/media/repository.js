import { Media } from '../../models';
import { queryBuilderGetList } from './query-builder'
import { ObjectId } from '../../utils/mongoose'

const createMedia = async (data) => {
  return Media.create(data)
};


const findOne = async (query) => {
  return Media.findOne(query);
};

const findById = async (id) => {
  return Media.findOne(new ObjectId(id));
};

async function findAll(request) {
  const { query, projection, options } = queryBuilderGetList(request)
  const [data, count] = await Promise.all([
    Media.find(query, projection, options).lean(),
    Media.countDocuments(query)
  ])
  return {
    total: count || 0,
    data: data || []
  }
}

export default {
  createMedia,
  findById,
  findOne,
  findAll
};

import { mongoose, Schema } from '../../utils/mongoose'
import { mediaType, mediasType } from './config'

const MediaSchema = new Schema({
  user: {
    _id: String,
    username: String, // is cms user
    email: String, // is app user
    phone: String // is app user
  },
  type: {
    type: String,
    enum: mediasType,
    defa: mediaType.image,
    required: true
  },
  origin: {
    type: String
  },
  feature: {
    type: String
  },
  thumbnail: {
    type: String
  },
  avatar: {
    type: String
  }
}, {
  versionKey: false,
  timestamps: true,
})

// Export
export default mongoose.model('Media', MediaSchema, 'medias')

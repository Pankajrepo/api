import { commonLocale } from '../../locales'
import service from './service'
import responseBuilder from '../../utils/response-builder'
import errorUtil from '../../utils/error'
import to from '../../utils/to'
import { BAD_REQUEST_CODE } from '../system/authorizator';
import { upload, uploadAvatar } from '../../utils/multer'
import { handleResponse } from '../../utils/handle-response';

const uploadImage = async (req, res) => {
  upload.array('images')(req, res, async (err) => {
    if (err) {
      err.code = BAD_REQUEST_CODE
      return res.status(BAD_REQUEST_CODE).jsonp(responseBuilder.build(false, {}, errorUtil.parseError(err)))
    }
    if (req.invalidFile && req.files.length < 1) {
      return res.status(BAD_REQUEST_CODE).jsonp(responseBuilder.build(false, {}, errorUtil.parseError(commonLocale.invalidDataRequest)))
    }
    if (req.files && req.files.length > 0) {
      const [error, medias] = await to(service.uploadImage(req.files, req.user));
      return handleResponse(error, medias[0], req, res);
    }
    handleResponse(null, null, req, res);
  })
}
/**
 * Unauthen,limit size 1Mb
 * @param {*} req
 * @param {*} res
 */
const uploadAvatarRegister = async (req, res) => {
  uploadAvatar.array('images')(req, res, async (err) => {
    if (err) {
      err.code = BAD_REQUEST_CODE
      return res.status(BAD_REQUEST_CODE).jsonp(responseBuilder.build(false, {}, errorUtil.parseError(err)))
    }
    if (req.invalidFile && req.files.length < 1) {
      return res.status(BAD_REQUEST_CODE).jsonp(responseBuilder.build(false, {}, errorUtil.parseError(commonLocale.invalidDataRequest)))
    }
    if (req.files && req.files.length > 0) {
      const [error, medias] = await to(service.uploadImage(req.files, null));
      return handleResponse(error, medias[0], req, res);
    }
    handleResponse(null, null, req, res);
  })
}

const list = async (req, res) => {
  const [error, result] = await to(service.index(req.query))
  handleResponse(error, result, req, res)
}

export default {
  uploadImage,
  list,
  uploadAvatarRegister
}

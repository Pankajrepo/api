import express from 'express'
import MediaController from './controller'
import authentication from '../../middleware/authMiddleWare'

const router = express.Router()

router.post('/', authentication, MediaController.uploadImage)
router.get('/', authentication, MediaController.list)
// router.post('/avatar', MediaController.uploadAvatarRegister)

export default router

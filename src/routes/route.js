import { Router } from 'express'
import authenticator from '../packages/system/authenticator'
import commonRouter from '../packages/common/route'
import mediaRouter from '../packages/media/route'
import { userRouter, authRouter } from '../packages/user/route'

const api = Router();

api.use('*', authenticator)
api.use('/auth', authRouter)
api.use('/user', userRouter)
api.use('/media', mediaRouter)
api.use('/common', commonRouter)

export default api

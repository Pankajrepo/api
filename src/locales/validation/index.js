import user from './user.js'
import phone from './phone.js'
import email from './email.js'


export default {
  user,
  phone,
  email
}

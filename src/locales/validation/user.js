import { commonCode } from '../response-code'

export default {
  accountBlocked: {
    code: commonCode.badRequest,
    message: 'user.accountBlocked'
  },
  existUser: {
    code: commonCode.badRequest,
    message: 'user.existUser'
  },
  passwordNotMatch: {
    code: commonCode.badRequest,
    message: 'user.passwordNotMatch'
  }
}

import { commonCode } from '../response-code'

export default {
  phoneInvalid: {
    code: commonCode.badRequest,
    message: 'phone.phoneInvalid'
  },
  expiredCode: {
    code: commonCode.badRequest,
    message: 'phone.expiredCode'
  },
  phoneNoneRegister: {
    code: commonCode.badRequest,
    message: 'phone.phoneNoneRegister'
  },
  incorrectCode: {
    code: commonCode.badRequest,
    message: 'phone.incorrectCode'
  },
  sendSMSFail: {
    code: commonCode.badRequest,
    message: 'phone.sendSMSFail'
  },
  existedPhone: {
    code: commonCode.duplicate,
    message: 'phone.existedPhone'
  },
  phoneNotVerified: {
    code: commonCode.badRequest,
    message: 'phone.phoneNotVerified'
  }
}

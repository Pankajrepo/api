export default {
  db: 'mongodb://localhost/dual_lite_dev',
  url: 'http://localhost:4444',
  media_url: 'https://dual-lite-media-dev-bucket.s3.ap-south-1.amazonaws.com',
  dbOptions: (options) => {
    return {
      useCreateIndex: true,
      autoIndex: options.autoIndex,
      // autoReconnect: true,
      useFindAndModify: false,
      useUnifiedTopology: true,
      useNewUrlParser: true,
      keepAlive: 1,
      connectTimeoutMS: 300000,
      socketTimeoutMS: 300000
    }
  },
  // Secret for token
  secret: '8?@B##o!fV}5R8fsdf*&*G',
  email: {
    id: 'dev@dual-lite.com',
    pass: 'kdjhfkdshf3478384'
  },
  superAdmin: {
    username: 'admin',
    password: 'admin!321',
    email: 'admin@dual-lite.com',
    fullName: 'Super Admin'
  },
  redisDB: {
    host: 'localhost',
    port: 6379
  }
}

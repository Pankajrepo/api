export default {
  db: 'mongodb://localhost/_db',
  dbOptions: (options) => {
    return {
      useUnifiedTopology: true,
      useCreateIndex: true,
      autoIndex: options.autoIndex,
      useNewUrlParser: true,
      keepAlive: 1,
      connectTimeoutMS: 300000,
      socketTimeoutMS: 300000
    }
  },
  secret: process.env.SECRET,
  email: {
    id: 'prod@dual-lite.com',
    pass: process.env.EMAIL_PASS
  },
  superAdmin: {
    username: process.env.SUPER_ADMIN_USERNAME,
    password: process.env.SUPER_ADMIN_PASSWORD,
    email: process.env.SUPER_ADMIN_EMAIL,
    fullName: process.env.SUPER_ADMIN_FULL_NAME
  },
  redisDB: {
    host: process.env.REDIS_HOST,
    port: 6379
  },
  masterIP: '17.3.41.46'
}

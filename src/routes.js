import { Router } from 'express'
import routes from './routes/route'

export default () => {
  const api = Router();

  api.use('/api/v1/', routes)

  return api
}

const sio = require('socket.io');

let io = null;
/**
 * Emit to all user activity screen on 'u_{id}' room
 * First, on connection need join all user socketid to room 'u_{id}: id of user model'
 * @param room 'u_{id}' room name
 * @param topic name of event 'ninja_assiged'
 * @param message data type any
 */
exports.touser = function (room, topic, message) {
  if (!room || !topic || !io) {
    return
  }
  io.to(room).emit(topic, message)
}
/**
 * @param room ex:'notifications' room name
 * @param topic name of event 'notify'
 * @param message data type any
 */
exports.toroom = function (room, topic, message) {
  if (!room || !topic || !io) {
    return
  }
  io.to(room).emit(topic, message)
}
/**
 * Send message to all client socket
 * @param topic name of event 'notify'
 * @param message data type any
 */
exports.toall = function (topic, message) {
  if (!topic || !io) {
    return
  }
  io.emit(topic, message)
}
exports.io = function () {
  return io
}
exports.initialize = function (server) {
  io = sio(server)
  return io
};

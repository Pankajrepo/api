import aws from 'aws-sdk'
import multer from 'multer'
import sharp from 'sharp'
import multerS3 from 'multer-s3-transform'
import path from 'path'
import crypto from 'crypto'
import config from '../configs'
import helper from './helper'
import { reverseString } from './string'

const date = helper.getDate();
const hour = helper.getHour()

aws.config.update({
  secretAccessKey: process.env.AWS_BUCKET_SECRET_KEY,
  accessKeyId: process.env.AWS_BUCKET_ACCESS_KEY,
  region: process.env.AWS_BUCKET_REGION
})

const s3 = new aws.S3()
const maxSize = 5 * 1024 * 1024;
const maxSizeAvatar = 1 * 1024 * 1024;
const formatError = config.errorResponse.format
const { invalidFile } = config.errorResponse
const time = helper.getTime()
const multerOptions = {
  storage: multerS3({
    s3,
    bucket: process.env.AWS_BUCKET,
    acl: 'public-read',
    limits: {
      fileSize: maxSize,
      files: 5
    },
    contentType: multerS3.AUTO_CONTENT_TYPE,
    shouldTransform: (req, file, cb) => {
      cb(null, /^image/i.test(file.mimetype))
    },
    transforms: [
      {
        id: 'origin',
        key: (req, file, cb) => {
          const encodeFileName = crypto.createHash('md5').update(file.originalname).digest('hex');
          const fileName = reverseString(encodeFileName + time)
          const compactName = crypto.createHash('md5').update(fileName).digest('hex');
          const fileNameToSave = `public/images/${date}/${hour}/${compactName}_origin`
          cb(null, fileNameToSave)
        },
        transform: (req, file, cb) => {
          cb(null, sharp().withMetadata())
        }
      }, {
        id: 'thumbnail',
        key: (req, file, cb) => {
          const encodeFileName = crypto.createHash('md5').update(file.originalname).digest('hex');
          const fileName = reverseString(encodeFileName + time)
          const compactName = crypto.createHash('md5').update(fileName).digest('hex');
          const fileNameToSave = `public/images/${date}/${hour}/${compactName}_800x800`
          cb(null, fileNameToSave)
        },
        transform: (req, file, cb) => {
          cb(
            null,
            sharp()
              .resize(config.imageDimensions.thumbnail)
              .withMetadata()
          )
        }
      },
      {
        id: 'feature',
        key: (req, file, cb) => {
          const encodeFileName = crypto.createHash('md5').update(file.originalname).digest('hex');
          const fileName = reverseString(encodeFileName + time)
          const compactName = crypto.createHash('md5').update(fileName).digest('hex');
          const fileNameToSave = `public/images/${date}/${hour}/${compactName}_1200`
          cb(null, fileNameToSave)
        },
        transform: (req, file, cb) => {
          cb(
            null,
            sharp()
              .resize(config.imageDimensions.feature)
              .withMetadata()
          )
        }
      },
      {
        id: 'avatar',
        key: (req, file, cb) => {
          const encodeFileName = crypto.createHash('md5').update(file.originalname).digest('hex');
          const fileName = reverseString(encodeFileName + time)
          const compactName = crypto.createHash('md5').update(fileName).digest('hex');
          const fileNameToSave = `public/images/${date}/${hour}/${compactName}_200x200`
          cb(null, fileNameToSave)
        },
        transform: (req, file, cb) => {
          cb(
            null,
            sharp()
              .resize(config.imageDimensions.avatar)
              .withMetadata()
          )
        }
      }
    ]
  }),
  storageAvatar: multerS3({
    s3,
    bucket: process.env.AWS_BUCKET,
    acl: 'public-read',
    limits: {
      fileSize: maxSizeAvatar,
      files: 1
    },
    contentType: multerS3.AUTO_CONTENT_TYPE,
    shouldTransform: (req, file, cb) => {
      cb(null, /^image/i.test(file.mimetype))
    },
    transforms: [
      {
        id: 'avatar',
        key: (req, file, cb) => {
          const encodeFileName = crypto.createHash('md5').update(file.originalname).digest('hex');
          const fileName = reverseString(encodeFileName + helper.getTime())
          const compactName = crypto.createHash('md5').update(fileName).digest('hex');
          const fileNameToSave = `public/images/${date}/${hour}/${compactName}_200x200`
          cb(null, fileNameToSave)
        },
        transform: (req, file, cb) => {
          cb(
            null,
            sharp()
              .resize(config.imageDimensions.avatar)
              .withMetadata()
          )
        }
      }
    ]
  })
};

const upload = multer({ storage: multerOptions.storage, fileFilter, limits: { fileSize: config.fileSize.limit } })

function fileFilter(req, file, callback) {
  const ext = path.extname(file.originalname)
  if (ext.toLowerCase() === '.svg') {
    req.fileValidationError = formatError

    return callback(null, false, new Error(formatError))
  }
  if (!['.png', '.jpg', '.gif', '.jpeg'].includes(ext.toLowerCase())) {
    req.invalidFile = invalidFile

    return callback(null, false)
  }

  callback(null, true);
}


const uploadAvatar = multer({ storage: multerOptions.storageAvatar, fileFilter, limits: { fileSize: maxSizeAvatar } })

exports.upload = upload
exports.uploadAvatar = uploadAvatar

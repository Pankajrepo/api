/* eslint-disable no-unused-vars */
import mongoose from 'mongoose';


const mongoURL = process.env.MONGO_URL || 'mongodb://localhost/go_safe_dev';

/**
 * Seeders List
 * order is important
 * @type {Object}
 */
export const seedersList = {

};
/**
 * Connect to mongodb implementation
 * @return {Promise}
 */
export const connect = async () =>
  await mongoose.connect(mongoURL, { useNewUrlParser: true });
/**
 * Drop/Clear the database implementation
 * @return {Promise}
 */
export const dropdb = async () => mongoose.connection.db.dropDatabase();

